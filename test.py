import requests as req
import sys
import json

'''
Enter the correct API key below. Without it, authorization will fail and no requests will work 
'''
API_KEY = "29138762-9149-41bd-a89e-00d87d7ec7e2"

url = 'http://localhost:1356/api'
headers = {"Authorization": API_KEY}

tests = ["",
         "Authentication with no credentials",
         "API POST/GET",
         "GET by id",
         "Pairwise POST",
         "Updating",
         "Deletion",
         "Filter by id",
         "Complex filter",
         "Sort by id",
         "Sort by -id",
         "Complex sort"
         ]

pairwise_params_post = [
    {"user": 1, "title": "I should pass", "description": "Passed", "state": 0, "deadline": "2012-03-05T15:54:04"},
    {"user": 1, "title": "", "description": "", "state": 1, "deadline": "asdf"},
    {"user": 1, "title": "Test task 2", "description": "Just a small test", "state": "abc",
     "deadline": "2012-03-05T15:54:04"},
    {"user": -1, "title": "ASDFASDFASADF", "description": "", "state": "abc", "deadline": 3},
    {"user": 'ben', "title": "", "description": "I should fail", "state": 2, "deadline": "2012-03-05T15:54:04"},
    {"user": 2, "title": "", "description": "", "state": 2, "deadline": "rererere"},
    {"user": 2, "title": "I should fail too", "description": "oops", "state": -5, "deadline": "2012-03-05T15:54:04"},
    {"user": None, "title": "I should fail", "description": "", "state": 1.1, "deadline": -1.1},
    {"user": None, "title": "", "description": "Zazazaz", "state": 1, "deadline": "2012-03-05T15:54:04"}
]

pairwise_params_put = [
    {"user": 1, "title": "PUT a different title", "description": "", "state": 1, "deadline": "2016-05-03T15:54:04"},
    {"user": 1, "title": "", "description": "Fail", "state": -1, "deadline": "qwer"},
    {"user": -1, "title": "", "description": "Fail", "state": "abc", "deadline": "2012-03-05T15:54:04"},
    {"user": "abc", "title": "Fail", "description": "Fail", "state": 2, "deadline": 6}
]

final_post = {"title": "Another different title", "description": "And description", "state": 3,
              "deadline": "2012-05-01T12:22:22"}


def getID(d):
    return d['id']


def extrID(l):
    temp = []
    for d in l:
        temp.append(getID(d))
    return temp


r = req.get(url, headers=headers, )

if r.text == '{"detail":"Authentication credentials were not provided."}':
    print("Authorization failed. Please use correct login credentials for the test")
    sys.exit()

start_db = json.loads(r.text)
start_db.sort(key=getID)
START_ID = 1
if len(start_db) > 0:
    START_ID = 1 + start_db[-1]['id']


'''  # Test 1 - Auth with no credentials # '''

r = req.get(url)
if r.text == '{"detail":"Authentication credentials were not provided."}':
    print(tests[1], "test passed")
else:
    print(tests[1], "test failed")

'''  # Test 2 - API POST/GET # '''

r = req.post(url, headers=headers, json={"title": "Test"})
r = req.get(url, headers=headers)

curr_db = json.loads(r.text)

if json.dumps(curr_db[-1]).replace(' ', '') == \
        '{"id":%i,"user":null,"title":"Test","description":null,"state":0,"deadline":null}' % START_ID:
    print(tests[2], "test passed")
else:
    print(tests[2], "test failed")

''' # Test 3 - GET by id # '''

r = req.get(url + '/%i' % START_ID, headers=headers)
if r.text == '{"id":%i,"user":null,"title":"Test","description":null,"state":0,"deadline":null}' % START_ID:
    print(tests[3], "test passed")
else:
    print(tests[3], "test failed")

''' # Test 4 - Pairwise POST # '''

t_pass = True

for i, testcase in enumerate(pairwise_params_post):
    r = req.post(url, headers=headers, json=testcase)

    # The only valid case
    if i == 0:
        if r.status_code != 201:
            t_pass = False
            print("\tStatus code for test case %i is %i, should be %i" % (i, r.status_code, 201))
        if (r.text != '{"id":%i,"user":1,"title":"I should pass","description":"Passed","state":0,'
                      '"deadline":"2012-03-05T15:54:04Z"}' % (START_ID + 1)):
            t_pass = False
            print("\tValid POSTed string doesn't match response")
        r1 = req.get(url + '/%i' % (START_ID + 1), headers=headers)
        if r1.text != r.text:
            print("\tPOST response doesn't match GET response on same non-mutated object")

    # The rest of invalid cases
    else:
        if r.status_code != 400:
            t_pass = False
            print("\tStatus code for test case %i is %i, should be %i" % (i, r.status_code, 400))

        ans = json.loads(r.text)
        if ('user' not in ans) and (i in [3, 4]):
            t_pass = False
            print("\tAPI didn't give an error on user when it should have: test case %i" % i)
        if ('user' in ans) and (i not in [3, 4]):
            t_pass = False
            print("\tAPT gave an error on user when it shouldn't have: test case %i" % i)
            if i in [5, 6]:
                print("\tMake sure you have created a user in admin dashboard before running the test")

        if ('title' not in ans) and (i in [1, 4, 5, 8]):
            t_pass = False
            print("\tAPI didn't give an error on title when it should have: test case %i" % i)
        if ('title' in ans) and (i not in [1, 4, 5, 8]):
            t_pass = False
            print("\tAPI gave an error on title when it shouldn't have: test case %i" % i)

        if ('state' not in ans) and (i in [2, 3, 6, 7]):
            t_pass = False
            print("\tAPI didn't give an error on state when it should have: test case %i" % i)
        if ('state' in ans) and (i not in [2, 3, 6, 7]):
            t_pass = False
            print("\tAPI gave an error on state when it shouldn't have: test case %i" % i)

        if ('deadline' not in ans) and (i in [1, 3, 5, 7]):
            t_pass = False
            print("\tAPI didn't give an error on deadline when it should have: test case %i" % i)
        if ('deadline' in ans) and (i not in [1, 3, 5, 7]):
            t_pass = False
            print("\tAPI gave an error on deadline when it shouldn't have: test case %i" % i)

if t_pass:
    print(tests[4], "test passed")
else:
    print(tests[4], "test failed")

''' # Test 5 - Updating # '''

t_pass = True

for i, testcase in enumerate(pairwise_params_put):
    r = req.put(url + '/%i/' % START_ID, headers=headers, json=testcase)

    # Valid test case: 1
    if i == 0:
        if r.status_code != 200:
            t_pass = False
            print("\tReturn status code on test case %i is %i. Expected %i" % (i, r.status_code, 200))
        if r.text != '{"id":%i,"user":1,"title":"PUT a different title","description":"","state":1,' \
                     '"deadline":"2016-05-03T15:54:04Z"}' % START_ID:
            t_pass = False
            print("\tValid test case response doesn't match its update json parameters")

    # Invalid test cases: rest
    else:
        if r.status_code != 400:
            t_pass = False
            print("\tReturn status code on test case %i is %i. Expected %i" % (i, r.status_code, 400))

        ans = json.loads(r.text)
        if ('user' not in ans) and (i in [2, 3]):
            t_pass = False
            print("\tAPI didn't give an error on user when it should have: test case %i" % i)
        if ('user' in ans) and i == 1:
            t_pass = False
            print("\tAPI gave an error on user when it shouldn't have: test case %i" % i)

        if ('title' not in ans) and (i in [1, 2]):
            t_pass = False
            print("\tAPI didn't give an error on title when it should have: test case %i" % i)
        if ('title' in ans) and i == 3:
            t_pass = False
            print("\tAPI gave an error on title when it shouldn't have: test case %i" % i)

        if ('state' not in ans) and (i in [1, 2]):
            t_pass = False
            print("\tAPI didn't give an error on state when it should have: test case %i" % i)
        if ('state' in ans) and i == 3:
            t_pass = False
            print("\tAPI gave an error on state when it shouldn't have: test case %i" % i)

        if ('deadline' not in ans) and (i in [1, 3]):
            t_pass = False
            print("\tAPI didn't give an error on deadline when it should have: test case %i" % i)
        if ('deadline' in ans) and i == 2:
            t_pass = False
            print("\tAPI gave an error on deadline when it shouldn't have: test case %i" % i)

if t_pass:
    print(tests[5], "test passed")
else:
    print(tests[5], "test failed")

''' # Test 6 - Deletion # '''

r = req.delete(url + '/%i/' % START_ID, headers=headers)
r = req.get(url + '/%i/' % START_ID, headers=headers)

if r.text != '{"id":%i,"user":1,"title":"PUT a different title","description":"","state":3,' \
             '"deadline":"2016-05-03T15:54:04Z"}' % START_ID:
    print(tests[6], "test passed")
else:
    print(tests[6], "test failed")

''' # Test 7 - Filter by id # '''

filter_text_gte = (
            '[{"id":%i,"user":1,"title":"I should pass","description":"Passed","state":0,"deadline":"2012-03-05T15:54:04Z"},{"id":%i,"user":1,"title":"PUT a different title","description":"","state":1,"deadline":"2016-05-03T15:54:04Z"}]' % (
    START_ID + 1, START_ID))

r = req.get(url + '?filter=id__gte=%i' % START_ID, headers=headers)
if r.text == filter_text_gte:
    print(tests[7], "test passed")
else:
    print(tests[7], "test failed")

''' # Test 8 - Complex filter # '''

t_pass = True
filter_text = '[{"id":%i,"user":1,"title":"PUT a different title","description":"","state":1,' \
              '"deadline":"2016-05-03T15:54:04Z"}]' % START_ID

r = req.get(url + '?filter=id__gte=%i:asdf' % START_ID, headers=headers)
if r.text != '[]':
    t_pass = False
    print ("\tComplex filter test failed: :asdf in query should return empty set")

r = req.get(url + '?filter=id__gte=%i:asdf=qwer' % START_ID, headers=headers)
if r.text != '[]':
    t_pass = False
    print("\tComplex filter test failed: :asdf=qwer in query should return empty set")

r = req.get(url + '?filter=id__gte=%i:id=%i' % (START_ID, START_ID), headers=headers)
if r.text != filter_text:
    t_pass = False
    print("\tComplex filter test failed: id=%i doesn't match expected object" % START_ID)

r = req.get(url + '?filter=id__gte=%i:user=1' % START_ID, headers=headers)
if r.text != filter_text_gte:
    t_pass = False
    print("\tComplex filter test failed: user=1 doesn't match expected objects")

r = req.get(url + '?filter=id__gte=%i:title__contains=title' % START_ID, headers=headers)
if r.text != filter_text:
    t_pass = False
    print("\tComplex filter test failed: title contains 'title' doesn't match expected object")

r = req.get(url + '?filter=id__gte=%i:description=' % START_ID, headers=headers)
if r.text != filter_text:
    t_pass = False
    print("\tComplex filter test failed: description=[empty] doesn't match expected object")

r = req.get(url + '?filter=id__gte=%i:state__gt=0' % START_ID, headers=headers)
if r.text != filter_text:
    t_pass = False
    print("\tComplex filter test failed: state>0 doesn't match expected object")

r = req.get(url + '?filter=id__gte=%i:deadline__lt=2017-05-12' % START_ID, headers=headers)
if r.text != filter_text_gte:
    t_pass = False
    print("\tComplex filter test failed: deadline<2017-05-12 doesn't match expected objects")

if t_pass:
    print(tests[8], "test passed")
else:
    print(tests[8], "test failed")

''' # Test 9 - Sort by id # '''

r = req.post(url, headers=headers, json=final_post)

r = req.get(url + '?filter=id__gte=%i&sort=id' % START_ID, headers=headers)

if extrID(json.loads(r.text)) == [START_ID, START_ID + 1, START_ID + 2]:
    print(tests[9], "test passed")
else:
    print(tests[9], "test failed")

''' # Test 9 - Sort by -id # '''

r = req.get(url + '?filter=id__gte=%i&sort=-id' % START_ID, headers=headers)

if extrID(json.loads(r.text)) == [START_ID + 2, START_ID + 1, START_ID]:
    print(tests[10], "test passed")
else:
    print(tests[10], "test failed")

''' # Test 10 - Complex sort # '''

t_pass = True

r = req.get(url + '?filter=id__gte=%i&sort=user:-id' % START_ID, headers=headers)
if extrID(json.loads(r.text)) != [START_ID + 1, START_ID, START_ID + 2]:
    t_pass = False
    print ("\tComplex test case failed: order by USER THEN -ID is expected to be", [START_ID + 1, START_ID, START_ID + 2], " actual ", extrID(json.loads(r.text)))

r = req.get(url + '?filter=id__gte=%i&sort=title' % START_ID, headers=headers)
if extrID(json.loads(r.text)) != [START_ID + 2, START_ID + 1, START_ID]:
    t_pass = False
    print ("\tComplex test case failed: order by TITLE is expected to be", [START_ID + 2, START_ID + 1, START_ID], " actual ", extrID(json.loads(r.text)))

r = req.get(url + '?filter=id__gte=%i&sort=description' % START_ID, headers=headers)
if extrID(json.loads(r.text)) != [START_ID, START_ID + 2, START_ID + 1]:
    t_pass = False
    print ("\tComplex test case failed: order by DESCRIPTION is expected to be", [START_ID, START_ID + 2, START_ID + 1], " actual ", extrID(json.loads(r.text)))

r = req.get(url + '?filter=id__gte=%i&sort=-state' % START_ID, headers=headers)
if extrID(json.loads(r.text)) != [START_ID + 2, START_ID, START_ID + 1]:
    t_pass = False
    print ("\tComplex test case failed: order by -STATE is expected to be", [START_ID + 2, START_ID, START_ID + 1], " actual ", extrID(json.loads(r.text)))

r = req.get(url + '?filter=id__gte=%i&sort=deadline' % START_ID, headers=headers)
if extrID(json.loads(r.text)) != [START_ID + 1, START_ID + 2, START_ID]:
    t_pass = False
    print ("\tComplex test case failed: order by DEADLINE is expected to be", [START_ID + 1, START_ID + 2, START_ID], " actual ", extrID(json.loads(r.text)))

if t_pass:
    print(tests[11], "test passed")
else:
    print(tests[11], "test failed")

print("Tests finished")
