## Description

Project created as a developer exercise for a company position.

The specifications were:
1. Use Python, Django, docker-compose, Docker Engine, GUnicorn, NGINX, PostgreSQL in the stack
2. Create a server app serving TODO Tasks with set fields
3. Serve tasks through a REST API
4. Provide authorization for the API with API keys
5. Create users through Django's Admin Dashboard. The dashboard should provide API keys for users
6. Write a test testing the API from the user's standpoint (from python console, separate from Docker container), without using the Django unit tests
7. Deploy the app using 3 containers: Django/Gunicorn; PostgreSQL; NGINX

## Requirements
Python 3.8

docker-compose:
<https://github.com/docker/compose/releases/tag/1.28.6>

## Running the server

#### Development version
To start the server:
```
docker-compose -f docker-compose.yml up -d --build
```

To stop the server:
```
docker-compose -f docker-compose.yml down -v
```

Run all other commands through the docker-compose front. For example, to create a superuser:

```
docker-compose exec web python manage.py createsuperuser
```

#### Production version

To start the server:
```
docker-compose -f docker-compose.prod.yml up -d --build
```

To stop the server:
```
docker-compose -f docker-compose.prod.yml down -v
```

### After starting the server

Run all other commands through the docker-compose front. For example, to create a superuser:

```
docker-compose exec web python manage.py createsuperuser
```


API is accessed through <localhost:1356/api>

The API requires a user API key, or the user to be logged in the browser in order to work.

The first user can be created by the Django command:
```
docker-compose exec web python manage.py createsuperuser
```

If on browser, press "Login" in the upper right corner of the page to login into the system.

If accessing API otherwise (running tests, for example), a key will be neccessary. To get the key:

1. Create a superuser
2. Login into Django's admin system at <localhost:1356/admin>
3. Select the Users table under TODOTASK_API administration
4. An API key should be displayed with other user info. Copy it and insert into the header of your requests


### API

#### Requests

The API supports the following requests: GET, POST, PUT, DELETE

#### GET

_GET /api_ returns list of all todo tasks currently on the server

_GET /api/x_ returns a todo task with id _x_

Filters can be used with GET requests to filter by one or more values. 

To create a filter, add _filter=_ parameter in the URL, and add any fields you need to filter by, delimeted by a colon (:). Example:

```
GET /api?filter=user=1:state=0
```

Will return all tasks set to user with user id of _1_ and the state of _0_ (TODO)

Filters also support Django field lookups. Example:

```
GET /api?filter=id__gte=5:title__contains=Fix
```

Will return all tasks with an id greater or equal to _5_ and titles that contain the substring _Fix_

Queries can also be sorted through the sort parameter, colon separated:

```
GET /api?sort=-state:title
```

Will sort the resulting response by descending order of state, then by ascending order of title

#### POST

_POST /api_ passes a json with a new object to server.  Any attributes not set in the json will be set to null in the resulting object.

#### PUT

_PUT /api/x_ modifies the todo task with id _x_. Note that PUT rewrites __ALL__ attributes of the task, so the entire object must be copied to the json to modify only one attribute, otherwise attributes will be set to null

#### DELETE

_DELETE /api/x_ sets the state of the task with id _x_ to 'deleted'. Same state can be set with a PUT request

#### Attributes

1. __id__: numeric, starts from 1. Unique id for a task. Always set automatically by the server. Cannot be modified by users.
2. __user__: numeric, starts from 1. Primary key and id for a created user.
3. __title__: text. Cannot be empty. Title of the task.
4. __description__: text. Description of the task.
5. __state__: numeric, 0-3. Default: 0. Represents task state. 0:TODO; 1:In progress; 2:Done; 3:Deleted
6. __deadline__: datetime. Time by which the task can be completed. Tasks can be set to past dates as deadlines.

## Running tests

Please run the tests on the dev version to avoid trashing the database with test data.
1. Start the dev server:
```
docker-compose -f docker-compose.yml up -d --build
```

The tests rely on the database having at least 2 users, so make sure you create a superuser and a regular user before you run the tests.

2. Create a superuser:
```
docker-compose exec web python manage.py createsuperuser
```
3. Enter the admin dashboard at <localhost:1356/admin> and login
4. Navigate to Users in TODOTASK_API admin
5. Create a user in admin dashboard. Set the user to inactive.
6. Copy admin's API key to the API_KEY value in test.py (line 8).
7. Run the tests:
```
python test.py
```
Note that running tests populates the database with test TODOs. You may want to run the following to clear the database when you're done running all tests:
```
docker-compose exec web python manage.py flush --no-input
```
That will also delete all users created previously.