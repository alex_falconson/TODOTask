from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from .models import Todo, TodoUser
from .serializers import TodoSerializer
import re
from django.conf import settings


# Create your views here.

class ApiPermission(permissions.BasePermission):
    '''
    Global permission check based on API key
    '''

    def has_permission(self, request, view):

        if 'HTTP_AUTHORIZATION' in request.META:
            key = request.META['HTTP_AUTHORIZATION']
            api_match = TodoUser.objects.filter(api_key=key).exists()
            return api_match

        return False


class TodoListApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated | ApiPermission]

    # 1. List w/ filter
    def get(self, request, *args, **kwargs):
        '''
        List all the to do items for given requested user
        '''
        todos = Todo.objects.all()
        print("GET request data: ", request.query_params)

        if 'filter' in request.query_params:
            params = request.query_params['filter'].split(':')
            for sub in params:
                args = sub.split('=')
                try:
                    filt = {args[0]: args[1]}
                    todos = todos.filter(**filt)
                except:
                    todos = todos.none()

        if 'sort' in request.query_params:
            params = request.query_params['sort'].split(':')
            params = [item for item in params if re.match("-?id|-?user|-?title|-?description|-?state|-?deadline", item)]
            todos = todos.order_by(*params)

        serializer = TodoSerializer(todos, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 2. Create
    def post(self, request, *args, **kwargs):
        '''
        Create the To do with given to do data
        '''
        data = {
            'user': request.data.get('user'),
            'title': request.data.get('title'),
            'description': request.data.get('description'),
            'state': request.data.get('state'),
            'deadline': request.data.get('deadline'),
        }

        if data['state'] is None:
            data['state'] = 0

        serializer = TodoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TodoDetailApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated | ApiPermission]

    def get_object(self, todo_id):
        '''
        Helper method to get the object with given todo_id, and user_id
        '''
        try:
            return Todo.objects.get(id=todo_id)
        except Todo.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, todo_id, *args, **kwargs):
        '''
        Retrieves the To do with given todo_id
        '''
        todo_instance = self.get_object(todo_id)
        if not todo_instance:
            return Response(
                {"res": "Object with todo id does not exist"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = TodoSerializer(todo_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, todo_id, *args, **kwargs):
        '''
        Updates the to do item with given todo_id if exists
        '''
        todo_instance = self.get_object(todo_id)
        if not todo_instance:
            return Response(
                {"res": "Object with todo id does not exist"},
                status=status.HTTP_400_BAD_REQUEST
            )

        data = {
            'user': request.data.get('user'),
            'title': request.data.get('title'),
            'description': request.data.get('description'),
            'state': request.data.get('state'),
            'deadline': request.data.get('deadline'),
        }
        serializer = TodoSerializer(instance=todo_instance, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 5. Delete
    def delete(self, request, todo_id, *args, **kwargs):
        '''
        Deletes the to do item with given todo_id if exists
        '''
        todo_instance = self.get_object(todo_id)
        if not todo_instance:
            return Response(
                {"res": "Object with todo id does not exist"},
                status=status.HTTP_400_BAD_REQUEST
            )

        data = {
            'user': todo_instance.user,
            'title': todo_instance.title,
            'description': todo_instance.description,
            'state': 3,
            'deadline': todo_instance.deadline,
        }

        serializer = TodoSerializer(instance=todo_instance, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
