from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid


# Create your models here.

class TodoUser(AbstractUser):
    api_key = models.UUIDField(default=uuid.uuid4, editable=False)


class Todo(models.Model):
    TODO = 0
    IN_PROGRESS = 1
    DONE = 2
    DELETED = 3
    STATE_CHOICES = [
        (TODO, 'TODO'),
        (IN_PROGRESS, 'In progress'),
        (DONE, 'Done'),
        (DELETED, 'DELETED')
    ]

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(TodoUser, on_delete=models.PROTECT, blank=True, null=True)
    title = models.CharField(max_length=180)
    description = models.CharField(max_length=1000, blank=True, null=True)
    state = models.PositiveSmallIntegerField(choices=STATE_CHOICES, default=TODO)
    deadline = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '#' + str(self.id) + ': ' + self.title
