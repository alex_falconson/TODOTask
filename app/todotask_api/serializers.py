from rest_framework import serializers
from .models import Todo


class TodoSerializer(serializers.ModelSerializer):
    deadline = serializers.DateTimeField(allow_null=True)
    class Meta:
        model = Todo
        fields = ["id", "user", "title", "description", "state", "deadline"]
